'use strict'

var defaultify = require('../index')
  , __hasProp = {}.hasOwnProperty

require('chai').should()

describe('defaults', function() {

  it('returns the context function with default options', function() {
    var addAllProps = function (options) {
      var property, value
      return ((function () {
        var _results
        _results = []
        for (property in options) {
          if (!__hasProp.call(options, property)) { continue }
          value = options[property]
          _results.push(value)
        }
        return _results
      })()).reduce(function(sum, number) {
        return sum + number
      }, 0)
    }

    defaultify(addAllProps)
    addAllProps = addAllProps.withDefaults({
      a: 1,
      b: 2,
      c: 3
    })

    addAllProps().should.equal(6)
    addAllProps.should.have.property('withDefaults')
  })

})
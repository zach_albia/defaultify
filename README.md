# defaultify 
Allows a monadic function to dynamically give default values to properties to its "options" parameter using the added "withDefaults" function.


## Install

```bash
$ npm install --save git+https://bitbucket.org/zach_albia/defaultify.git
```


## Usage

```
#!javascript
var defaultify = require('defaultify')
  , fn = function (options) { }

fn = defaultify(fn)
fn.withDefaults({ defaultOpt: 'defaultValue' })
// each time fn is called, defaultOpt is merged with fn's options
```

## API

_(Coming soon)_


## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [gulp](http://gulpjs.com/).


## License

Copyright (c) 2014 Zach Albia. Licensed under the MIT license.
"use strict"

var _ = require('lodash')
  , def

function defaultify(fn) {
  return fn.withDefaults = _.partial(def, fn)
}

function defaults(fn, defaultOptions) {
  return (function (fn) {
    var fnWithDefaults
    fnWithDefaults = function (opts) {
      return fn(_.assign(defaultOptions, opts))
    }
    defaultify(fnWithDefaults)
    return fnWithDefaults
  })(fn)
}

def = defaults
module.exports = defaultify
